package Clickhouse

import (
	"fmt"
	"net/url"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
)

const (
	UnderlyingDriverName = "clickhouse"
)

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	source = config.Link

	if "" != config.Link {
		replaceSchemaPattern := `@(.+?)/([\w\.\-]+)+`

		if "" != config.Name {
			source, _ = gregex.ReplaceString(replaceSchemaPattern, "@$1/"+config.Name, config.Link)
			return
		}

		dbName, _ := gregex.MatchString(replaceSchemaPattern, config.Link)

		if 0 < len(dbName) {
			config.Name = dbName[len(dbName)-1]
		}

		return
	}

	if "" != config.Pass {
		source = fmt.Sprintf(
			"clickhouse://%s:%s@%s:%s/%s?debug=%t",
			config.User, url.PathEscape(config.Pass),
			config.Host, config.Port, config.Name, config.Debug,
		)
	} else {
		source = fmt.Sprintf(
			"clickhouse://%s@%s:%s/%s?debug=%t",
			config.User, config.Host, config.Port, config.Name, config.Debug,
		)
	}

	if "" != config.Extra {
		source = fmt.Sprintf("%s&%s", source, config.Extra)
	}

	return
}
