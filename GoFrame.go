package GoFrame

import (
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

const (
	DefaultCharset  = `utf8`
	DefaultProtocol = `tcp`
	LinkPattern     = `(\w+):([\w\-]*):(.*?)@(\w+?)\((.+?)\)/{0,1}([^\?]*)\?{0,1}(.*)`
)

//	@see: https://github.com/gogf/gf/blob/master/database/gdb/gdb_core_config.go
func ParseConfigNodeLink(node *gdb.ConfigNode) *gdb.ConfigNode {
	var match []string

	if "" != node.Link {
		match, _ = gregex.MatchString(LinkPattern, node.Link)
		matchSize := len(match)

		if 5 < matchSize {
			node.Type = match[1]
			node.User = match[2]
			node.Pass = match[3]
			node.Protocol = match[4]
			array := gstr.Split(match[5], ":")

			if 2 == len(array) && "file" != node.Protocol {
				node.Host = array[0]
				node.Port = array[1]
				node.Name = match[6]
			} else {
				node.Name = match[5]
			}

			if 6 < matchSize && "" != match[7] {
				node.Extra = match[7]
			}

			node.Link = ""
		}
	}

	if "" != node.Extra {
		data, _ := gstr.Parse(node.Extra)

		if 0 < len(data) {
			_ = gconv.Struct(data, &node)
		}
	}

	if "" == node.Charset {
		node.Charset = DefaultCharset
	}

	if "" == node.Protocol {
		node.Protocol = DefaultProtocol
	}

	return node
}

//	@see: https://github.com/gogf/gf/blob/master/database/gdb/gdb_core_config.go
func ParseConfigNode(node *gdb.ConfigNode) *gdb.ConfigNode {
	if "" != node.Link {
		node = ParseConfigNodeLink(node)
	}

	if "" != node.Link && "" == node.Type {
		match, _ := gregex.MatchString(`([a-z]+):(.+)`, node.Link)

		if len(match) == 3 {
			node.Type = gstr.Trim(match[1])
			node.Link = gstr.Trim(match[2])
		}
	}

	return node
}
