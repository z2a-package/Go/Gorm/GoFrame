package MicrosoftSQL

import (
	"fmt"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
	"github.com/gogf/gf/v2/text/gstr"
)

const (
	UnderlyingDriverName = "sqlserver"
)

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	if "" != config.Link {
		source = config.Link

		if "" != config.Name {
			source, _ = gregex.ReplaceString(`database=([\w\.\-]+)+`, "database="+config.Name, source)
		}

		return
	}

	source = fmt.Sprintf(
		"user id=%s;password=%s;server=%s;port=%s;database=%s;encrypt=disable",
		config.User, config.Pass, config.Host, config.Port, config.Name,
	)

	if "" != config.Extra {
		extraMap, throw := gstr.Parse(config.Extra)

		if nil != throw {
			return
		}

		for key, value := range extraMap {
			source += fmt.Sprintf(`;%s=%s`, key, value)
		}
	}

	return
}
