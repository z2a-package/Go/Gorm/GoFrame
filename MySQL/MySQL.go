package MySQL

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
)

const (
	UnderlyingDriverName = "mysql"
)

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	if "" != config.Link {
		source = config.Link

		if "" != config.Name {
			source, _ = gregex.ReplaceString(`/([\w\.\-]+)+`, "/"+config.Name, source)
		}

		return
	}

	source = fmt.Sprintf(
		"%s:%s@%s(%s:%s)/%s?charset=%s",
		config.User, config.Pass, config.Protocol, config.Host, config.Port, config.Name, config.Charset,
	)

	if "" != config.Timezone {
		if strings.Contains(config.Timezone, "/") {
			config.Timezone = url.QueryEscape(config.Timezone)
		}

		source = fmt.Sprintf("%s&loc=%s", source, config.Timezone)
	}

	if "" != config.Extra {
		source = fmt.Sprintf("%s&%s", source, config.Extra)
	}

	return
}
