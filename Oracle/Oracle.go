package Oracle

import (
	"fmt"
	"net"
	"net/url"
	"strconv"
	"strings"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

const (
	UnderlyingDriverName = "oracle"
)

func BuildUrl(server string, port int, service, user, password string, options map[string]string) (result string) {
	result = fmt.Sprintf("oracle://%s:%s@%s/%s", url.PathEscape(user), url.PathEscape(password), net.JoinHostPort(server, strconv.Itoa(port)), url.PathEscape(service))

	if nil != options {
		result += "?"

		for key, value := range options {
			value = strings.TrimSpace(value)

			for _, item := range strings.Split(value, ",") {
				item = strings.TrimSpace(item)

				if "SERVER" == strings.ToUpper(key) {
					result += fmt.Sprintf("%s=%s&", key, item)
				} else {
					result += fmt.Sprintf("%s=%s&", key, url.QueryEscape(item))
				}
			}
		}

		result = strings.TrimRight(result, "&")
	}

	return result
}

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	options := map[string]string{
		"CONNECTION TIMEOUT": "60",
		"PREFETCH_ROWS":      "25",
	}

	if config.Debug {
		options["TRACE FILE"] = "oracle_trace.log"
	}

	if "" != config.Link {
		source = config.Link

		if "" != config.Name {
			source, _ = gregex.ReplaceString(`@(.+?)/([\w\.\-]+)+`, "@$1/"+config.Name, source)
		}

		return
	}

	if "" != config.Extra {
		extraMap, throw := gstr.Parse(config.Extra)

		if nil != throw {
			return
		}

		for key, value := range extraMap {
			options[key] = gconv.String(value)
		}
	}

	source = BuildUrl(
		config.Host, gconv.Int(config.Port), config.Name, config.User, config.Pass, options,
	)

	return
}
