package PostgreSQL

import (
	"fmt"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/text/gregex"
	"github.com/gogf/gf/v2/text/gstr"
)

const (
	UnderlyingDriverName = "postgres"
)

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	if "" != config.Link {
		source = config.Link

		if "" != config.Name {
			source, _ = gregex.ReplaceString(`dbname=([\w\.\-]+)+`, "dbname="+config.Name, source)
		}

		return
	}

	if "" != config.Name {
		source = fmt.Sprintf(
			"user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
			config.User, config.Pass, config.Host, config.Port, config.Name,
		)
	} else {
		source = fmt.Sprintf(
			"user=%s password=%s host=%s port=%s sslmode=disable",
			config.User, config.Pass, config.Host, config.Port,
		)
	}

	if "" != config.Namespace {
		source = fmt.Sprintf("%s search_path=%s", source, config.Namespace)
	}

	if "" != config.Timezone {
		source = fmt.Sprintf("%s timezone=%s", source, config.Timezone)
	}

	if "" != config.Extra {
		extraMap, throw := gstr.Parse(config.Extra)

		if nil != throw {
			return
		}

		for key, value := range extraMap {
			source += fmt.Sprintf(` %s=%s`, key, value)
		}
	}

	return
}
