# GoFrame

Parse GoFrame DB config to GORM DSN

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/z2a-package/Go/Gorm/GoFrame.svg)](https://pkg.go.dev/gitlab.com/z2a-package/Go/Gorm/GoFrame)

Support Database:

- SQLite
- Oracle
- PostgreSQL
- MySQL
- MicrosoftSQL
- Clickhouse

Example:

```go
package main

import (
	"fmt"

	"gitlab.com/z2a-package/Go/Gorm/GoFrame"
	"gitlab.com/z2a-package/Go/Gorm/GoFrame/PostgreSQL"
	"github.com/gogf/gf/v2/database/gdb"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	config := &gdb.ConfigNode{
		Host: "127.0.0.1",
		Port: "5432",
		User: "postgres",
		Pass: "",
		Name: "postgres",
	}

	config = GoFrame.ParseConfigNode(config)
	source := PostgreSQL.ParseDataSourceName(config)
	dialector := postgres.Open(source)
	_, throw := gorm.Open(dialector)

	if nil != throw {
		fmt.Println("open database fail")
	} else {
		fmt.Println("open database success")
	}

}



```
