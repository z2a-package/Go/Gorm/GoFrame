package SQLite

import (
	"fmt"

	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/encoding/gurl"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

const (
	UnderlyingDriverName = "sqlite"
)

func ParseDataSourceName(config *gdb.ConfigNode) (source string) {
	if config.Link != "" {
		source = config.Link
	} else {
		source = config.Name
	}

	absolutePath, _ := gfile.Search(source)

	if "" != absolutePath {
		source = absolutePath
	}

	if "" != config.Extra {
		options := ""
		extraMap, throw := gstr.Parse(config.Extra)

		if nil != throw {
			return
		}

		for key, value := range extraMap {
			if options != "" {
				options += "&"
			}

			options += fmt.Sprintf(`_pragma=%s(%s)`, key, gurl.Encode(gconv.String(value)))
		}

		if len(options) > 1 {
			source += "?" + options
		}
	}

	return
}
